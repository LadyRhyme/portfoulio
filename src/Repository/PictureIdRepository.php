<?php

namespace App\Repository;

use App\Entity\PictureId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PictureId|null find($id, $lockMode = null, $lockVersion = null)
 * @method PictureId|null findOneBy(array $criteria, array $orderBy = null)
 * @method PictureId[]    findAll()
 * @method PictureId[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PictureIdRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PictureId::class);
    }

    // /**
    //  * @return PictureId[] Returns an array of PictureId objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PictureId
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
