/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.css';

require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');


// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

window.addEventListener("scroll", function(){
    var header = document.querySelector("header");
    header.classList.toggle("sticky", window.scrollY > 0);
})

// Leaflet Call //

var mymap = L.map('mapid').setView([45.5902219, 5.9184551], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibGFkeXJoeW1lIiwiYSI6ImNrZmQyaXoxZzFmNmMycW9mYzdidWtpZ3QifQ.VAouTPnLWxMvVORgCbDXvw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);
var marker = L.marker([45.5902219, 5.9184551]).addTo(mymap);
marker.bindPopup("<b>Coucou je suis un popup qui affiche le lieu où je suis ! :D</b>").openPopup();


